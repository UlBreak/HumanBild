import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Person Ula = new Person(170, 65.6f);
        System.out.println("\t>" + " Ульяна");
        Ula.say("Hi, you baka-baka");
        System.out.println("Рост Ульяны " + Ula.Height);
        System.out.println("Вес Ульяны " + Ula.Weight);

        System.out.println("\t>" + " Давид");
        Student David = new Student(210, 97.5f, 3); //С помошью конструктора
        System.out.println("Рост Давида " + Ula.Height + ".");
        System.out.println("Вес Давида " + Ula.Weight + ".");
        System.out.println("Давид перещёл на " + David.Course + " курс.");

        System.out.println("\t>" + " Илья");
        Student Ilya = new Student();
        Ilya.Height = 181; //Без помоши конструктора
        Ilya.Weight = 67.56f;
        System.out.println("Рост Ильи " + Ilya.Weight);
        System.out.println("Вес Ильи " + Ilya.Weight);
        Ilya.say("Я не учусь в университе, я работаю"); //в Student нет метода Say, но благодаря Student extends Person это работает
    }
}

/*
Что выведит консоль:
	> Ульяна
Hi, you baka-baka
Рост Ульяны 170
Вес Ульяны 65.6
	> Давид
Рост Давида 170.
Вес Давида 65.6.
Давид перещёл на 3 курс.
	> Илья
Рост Ильи 67.56
Вес Ильи 67.56
Я не учусь в университе, я работаю
 */